<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Event;
use AppBundle\Entity\Address;
use AppBundle\Form\Type\EventType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EventController extends Controller {


	/**
	 * @Route("/event/new")
	 */
	public function newAction(Request $request)
	{
		$event = new Event();
		$user = $this->getUser();
		$form = $this->createForm(EventType::class, $event);
		$form->add('submit', SubmitType::class, array(
            'label' => 'Create',
            'attr'  => array('class' => 'btn btn-primary col-md-offset-9')
        ));

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$event->setCreator($user);
			$em->persist($event);
			$em->flush();
			$this->addFlash(
            	'notice',
            	'Your event were saved!'
        	);

			return $this->redirect('/event'); 			
		}		

		return $this->render('AppBundle:Admin:new_event.html.twig',
			array('form' => $form->createView()));
	}

	/**
	 * @Route("/event")
	 *
	 */
	public function showAllAction()
	{
		$user   = $this->getUser();
		$em     = $this->getDoctrine()->getManager();
		$activeEvents = $em->getRepository('AppBundle:Event')->getActiveEvents($user->getId());

		return $this->render('AppBundle:Default:event_all.html.twig', array(
			'user' => $user,
			'activeEvents' => $activeEvents
		));
	}

	/**
	 * @Route("/event/edit/{id}")
	 *
	 */
	public function editAction($id, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$event = $em->getRepository('AppBundle:Event')->findOneById($id);
		$form = $this->createForm(EventType::class, $event);
				$form->add('submit', SubmitType::class, array(
            'label' => 'Create',
            'attr'  => array('class' => 'btn btn-primary col-md-offset-9')
        ));
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();		
			$em->persist($event);
			$em->flush();
			$this->addFlash(
            	'notice',
            	'Your changes were saved!'
        	);
			return $this->render('AppBundle:Admin:new_event.html.twig',
				array('form' => $form->createView()));			
		}		

		return $this->render('AppBundle:Admin:new_event.html.twig',
			array('form' => $form->createView()));
	}

	/**
	 * @Route('/add_user_to_group/{group_id}/{user_id}')
	 *
	 */
	// public function addUserToGroupAction($groupId, $userId, Request $request)
	// {

	// }

}