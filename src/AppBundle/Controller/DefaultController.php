<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends Controller
{
    /**
     * @Route("/index")
     * @Template("AppBundle:Default:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $contacts = [];
        $events   = [];
        $criteria = [];
        $user = $this->getUser();

        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

            // $contacts = $this->getDoctrine()
            //     ->getRepository('AppBundle:Person')
            //     ->findBy( $criteria, null, 5);
            // $events = $this->getDoctrine()
            //     ->getRepository('AppBundle:Event')
            //     ->findBy( $criteria, null, 5);
        }
                
        return $this->render('AppBundle:Default:index.html.twig', array(
             'user' => $user,
             'events' => null,
             'contacts' => null
        ));
    }

    /**
     * @Route("/admin")
     */
    public function adminAction(Request $request) 
    {
        return $this->render('AppBundle:Admin:admin_index.html.twig');
    }

    /**
     * @Route("/login", name="login")
     * @Template("AppBundle:Default:login.html.twig")
     */
    public function loginAction(Request $request)
    {
        $authUtils = $this->get('security.authentication_utils');
        return array(
            'username' => $authUtils->getLastAuthenticationError(),
            'erreur' => $authUtils->getLastUsername()
        );
    }
    
} 
