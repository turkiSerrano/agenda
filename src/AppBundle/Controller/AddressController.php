<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Address;
use AppBundle\Form\Type\AddressType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AddressController extends Controller {


	/**
	 * @Route("/address/new")
	 */
	public function newAction(Request $request)
	{
		$address = new Address();
		$form = $this->createForm(AddressType::class, $address);
		$form->add('submit', SubmitType::class, array(
            'label' => 'Create',
            'attr'  => array('class' => 'btn btn-primary col-md-offset-9')
        ));	
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$address->setUser($this->getUser());		
			$em->persist($address);
			$em->flush();
			$this->addFlash(
            	'notice',
            	'Your changes were saved!'
        	);
			return $this->render('AppBundle:Admin:new_address.html.twig',
				array('form' => $form->createView()));			
		}		

		return $this->render('AppBundle:Admin:new_address.html.twig',
			array('form' => $form->createView()));
	}

	/**
	 * @Route("/address/edit/{id}")
	 *
	 */
	public function editAction($id, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$address = $em->getRepository('AppBundle:Address')->findOneById($id);
		$form = $this->createForm(AddressType::class, $address);
		$form->add('submit', SubmitType::class, array(
            'label' => 'Create',
            'attr'  => array('class' => 'btn btn-primary col-md-offset-9')
        ));
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();		
			$em->persist($address);
			$em->flush();
			$this->addFlash(
            	'notice',
            	'Your changes were saved!'
        	);
			return $this->render('AppBundle:Admin:new_address.html.twig',
				array('form' => $form->createView()));			
		}		

		return $this->render('AppBundle:Admin:new_address.html.twig',
			array('form' => $form->createView()));
	}

}