<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Event;
use AppBundle\Entity\Person;
use AppBundle\Entity\Address;
use AppBundle\Form\Type\PersonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PersonController extends Controller {


	/**
	 * @Route("/person/new")
	 */
	public function newAction(Request $request)
	{
		$event = new Person();
		$form = $this->createForm(PersonType::class, $event);
		$form->add('submit', SubmitType::class, array(
            'label' => 'Create',
            'attr'  => array('class' => 'btn btn-primary col-md-offset-9')
        ));
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();		
			$em->persist($event);
			$em->flush();
			$this->addFlash(
            	'notice',
            	'Your changes were saved!'
        	);
			return $this->render('AppBundle:Admin:new_person.html.twig',
				array('form' => $form->createView()));			
		}		

		return $this->render('AppBundle:Admin:new_person.html.twig',
			array('form' => $form->createView()));
	}

	/**
	 * @Route("/person/edit/{id}")
	 *
	 */
	public function editAction($id, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$event = $em->getRepository('AppBundle:Person')->findOneById($id);
		$form = $this->createForm(PersonType::class, $event);
				$form->add('submit', SubmitType::class, array(
            'label' => 'Create',
            'attr'  => array('class' => 'btn btn-primary col-md-offset-9')
        ));
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();		
			$em->persist($event);
			$em->flush();
			$this->addFlash(
            	'notice',
            	'Your changes were saved!'
        	);
			return $this->render('AppBundle:Admin:new_person.html.twig',
				array('form' => $form->createView()));			
		}		

		return $this->render('AppBundle:Admin:new_person.html.twig',
			array('form' => $form->createView()));
	}
}