<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":true})
     */
    private $is_active;

    /**
     * @ORM\Column(type="simple_array", nullable=false)
     */
    private $roles;

    /**
     * @ORM\OneToOne(targetEntity="Address", mappedBy="user")
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="Event", mappedBy="users")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="creator")
     */
    private $createdEvents;

    public function getId()
    {
        return $this->id;
    }

    public function getEvents()
    {
        return $this->events;
    }

    public function setEvents($events)
    {
        $this->events = $events;
    }

    public function getCreatedEvents()
    {
        return $this->createdEvents;
    }

    public function setCreatedEvents($groups)
    {
        $this->createdEvents = $createdEvents;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function __construct()
    {
        $this->isActive = true;
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdGroups = \Doctrine\Common\Collections\ArrayCollection();
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    public function getSalt()
    {
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        // return json_encode(
        //     $this->id,
        //     $this->email,
        //     $this->username,
        //     $this->password,
        //     $this->is_active,
        //     // $this->salt,
        // );
        
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        // list (
        //     $this->id,
        //     $this->email,
        //     $this->username,
        //     $this->password,
        //     $this->is_active,
        //     $this->roles,
        //     // see section on salt below
        //     // $this->salt
        // ) = json_decode($serialized);
    }
}
