<?php 

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use WootBox\CatalogueBundle\Entity\Person;

class PersonType extends AbstractType
{
	
	public function buildForm(
		FormBuilderInterface $builder,
		array $options
	) {

		$builder
			->add('name', TextType::class)
			->add('age', IntegerType::class,array(
				'attr' => array('min' => 18, 'max' => 99, 'placeholder' => 18)
			))
			->add('firstname', TextType::class)
			->add('job', TextType::class);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Person',
        ));
    }
	

	public function getName()
	{
		return 'PersonType';
	}
}