<?php 

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use WootBox\CatalogueBundle\Entity\Event;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use AppBundle\Form\Type\AddressType;

class EventType extends AbstractType
{
	
	public function buildForm(
		FormBuilderInterface $builder,
		array $options
	) {

		$builder
			->add('title', TextType::class)
            ->add('date', DateTimeType::class);
	}
	
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Event',
        ));
    }

	public function getName()
	{
		return 'EventType';
	}
}