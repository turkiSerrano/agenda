<?php 

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use WootBox\CatalogueBundle\Entity\Address;

class AddressType extends AbstractType
{
	
	public function buildForm(
		FormBuilderInterface $builder,
		array $options
	) {

		$builder
			->add('num', IntegerType::class)
            ->add('street', TextType::class)
            ->add('zipCode', TextType::class)
            ->add('city', TextType::class)
            ->add('country', TextType::class);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Address',
        ));
    }
	

	public function getName()
	{
		return 'AddressType';
	}
}